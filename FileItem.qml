import QtQuick 2.0

Rectangle {
   x: 100; y: 100
   width: 128 + 16
   height: icon.height + (8 * 2) + fileToOpen.contentHeight + (8 * 2)
   border.width: 1

   property alias source: fileToOpen.text

   Drag.active: dragArea.drag.active

   Rectangle {
       id: icon
       width: 64; height: 64
       anchors.horizontalCenter: parent.horizontalCenter
       anchors.top: parent.top
       anchors.margins: 8
       color: "#00ff00"
   }

   Text {
       id: fileToOpen
       width: 128
       anchors.top: icon.bottom
       anchors.margins: 8
       anchors.horizontalCenter: parent.horizontalCenter
       horizontalAlignment: TextEdit.AlignHCenter
       wrapMode: TextEdit.WrapAnywhere

       text: "File to open ... "
   }

   MouseArea {
       id: dragArea
       anchors.fill: parent
       drag.target: parent
       onClicked: Qt.openUrlExternally(fileToOpen.text)
   }
}

import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import org.jucato 1.0 as Jucato

Rectangle {
    id: toolbar
    height: 96
    border.color: "black"; border.width: 1

    RowLayout {
        anchors.fill: parent

        spacing: 2

        Rectangle {
            color: "black"; Layout.minimumWidth: 80; Layout.minimumHeight: 80
            id: colorButton

            MouseArea {
                anchors.fill: parent
                onClicked: {
//                    canvas.color = parent.color
                    colorDialog.visible = true;
                }
            }

            ColorDialog {
                id: colorDialog
                title: "Please choose a color"
                onAccepted: {
                    canvas.color = colorDialog.color
                    colorButton.color = colorDialog.color
                }
                onRejected: {
                }
            }
        }

        Rectangle {
            id: pen05
            Layout.minimumWidth: 80; Layout.minimumHeight: 80
            border.color: "black"; border.width: 1

            MouseArea {
                anchors.fill: parent
                onClicked: canvas.penWidth = 5

                Rectangle {
                    width: 5; height: 5
                    color: "black"
                    radius: width * 0.5

                    anchors.centerIn: parent
                }
            }
        }

        Rectangle {
            id: pen10
            Layout.minimumWidth: 80; Layout.minimumHeight: 80
            border.color: "black"; border.width: 1

            MouseArea {
                anchors.fill: parent
                onClicked: canvas.penWidth = 10

                Rectangle {
                    width: 10; height: 10
                    color: "black"
                    radius: width * 0.5

                    anchors.centerIn: parent
                }
            }
        }

        Rectangle {
            id: pen20
            Layout.minimumWidth: 80; Layout.minimumHeight: 80
            border.color: "black"; border.width: 1

            MouseArea {
                anchors.fill: parent
                onClicked: canvas.penWidth = 20

                Rectangle {
                    width: 20; height: 20
                    color: "black"
                    radius: width * 0.5

                    anchors.centerIn: parent
                }
            }
        }

        Rectangle {
            id: drawMode
            border.color: "black"; Layout.minimumWidth: 80; Layout.minimumHeight: 80

            Text {
                text: "D"
                font.pointSize: 20
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: { canvas.mode = Jucato.Canvas.Draw; toolbar.state = "DRAW" }
            }
        }

        Rectangle {
            id: eraseMode
            border.color: "black"; Layout.minimumWidth: 80; Layout.minimumHeight: 80

            Text {
                text: "E"
                font.pointSize: 20
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: { canvas.mode = Jucato.Canvas.Erase; toolbar.state = "ERASE" }
            }
        }

        Rectangle {
            id: textMode
            border.color: "black"; Layout.minimumWidth: 80; Layout.minimumHeight: 80

            Text {
                text: "T"
                font.pointSize: 20
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: { canvas.mode = Jucato.Canvas.Text; toolbar.state = "TEXT"; }
            }
        }

        Rectangle {
            id: imageMode
            border.color: "black"; Layout.minimumWidth: 80; Layout.minimumHeight: 80

            Text {
                text: "I"
                font.pointSize: 20
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: { canvas.mode = Jucato.Canvas.Image; toolbar.state = "IMAGE" }
            }
        }
        Rectangle {
            id: fileMode
            border.color: "black"; Layout.minimumWidth: 80; Layout.minimumHeight: 80

            Text {
                text: "F"
                font.pointSize: 20
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: { canvas.mode = Jucato.Canvas.File; toolbar.state = "FILE" }
            }
        }
    }

    state: "DRAW"

    states: [
        State {
            name: "DRAW"
            PropertyChanges { target: drawMode; color: "lightgrey" }
            PropertyChanges { target: eraseMode; color: "white" }
            PropertyChanges { target: textMode; color: "white" }
            PropertyChanges { target: imageMode; color: "white" }
            PropertyChanges { target: fileMode; color: "white" }
        },

        State {
            name: "ERASE"
            PropertyChanges { target: eraseMode; color: "lightgrey" }
            PropertyChanges { target: drawMode; color: "white" }
            PropertyChanges { target: textMode; color: "white" }
            PropertyChanges { target: imageMode; color: "white" }
            PropertyChanges { target: fileMode; color: "white" }
        },
        State {
            name: "TEXT"
            PropertyChanges { target: textMode; color: "lightgrey" }
            PropertyChanges { target: drawMode; color: "white" }
            PropertyChanges { target: eraseMode; color: "white" }
            PropertyChanges { target: imageMode; color: "white" }
            PropertyChanges { target: fileMode; color: "white" }
        },
        State {
            name: "IMAGE"
            PropertyChanges { target: imageMode; color: "lightgrey" }
            PropertyChanges { target: textMode; color: "white" }
            PropertyChanges { target: drawMode; color: "white" }
            PropertyChanges { target: eraseMode; color: "white" }
            PropertyChanges { target: fileMode; color: "white" }
        },
        State {
            name: "FILE"
            PropertyChanges { target: fileMode; color: "lightgrey" }
            PropertyChanges { target: textMode; color: "white" }
            PropertyChanges { target: drawMode; color: "white" }
            PropertyChanges { target: eraseMode; color: "white" }
            PropertyChanges { target: imageMode; color: "white" }
        }
    ]
}

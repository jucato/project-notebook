#ifndef CANVAS_H
#define CANVAS_H

#include <QQuickPaintedItem>
#include <QImage>

class Canvas : public QQuickPaintedItem
{
    Q_OBJECT
    Q_ENUMS(Modes)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(int penWidth READ penWidth WRITE setPenWidth NOTIFY penWidthChanged)
    Q_PROPERTY(bool scribbling READ isScribbling WRITE setScribbling)
    Q_PROPERTY(Modes mode READ mode WRITE setMode NOTIFY modeChanged)

public:
    Canvas(QQuickItem* parent = 0);

    enum Modes { Draw, Erase, Text, Image, File };

    QColor color() const { return m_penColor; }
    void setColor(const QColor& newColor);

    int penWidth() const { return m_penWidth; }
    void setPenWidth(const int& newPenWidth);

    bool isScribbling() const { return m_scribbling; }
    void setScribbling(bool scribbling);

    Modes mode() const { return m_currentMode; }
    void setMode(Modes newMode);

Q_SIGNALS:
    void colorChanged();
    void eraseColorChanged();
    void penWidthChanged();
    void paintingStopped();
    void paintingStarted();
    void modeChanged();

protected:
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void paint(QPainter* painter);

private:
    void drawLineTo(const QPointF &endPoint);

    QColor m_penColor;
    int m_penWidth;
    QImage m_image;

    bool m_scribbling;
    QPointF m_lastPoint;

    Modes m_currentMode;
    Modes m_previousMode;
};

#endif // CANVAS_H

import QtQuick 2.0
import QtQuick.Controls 1.4

TextArea {
    id: textEdit

    property int maxWidth: font.pointSize * 10

    width: Math.max(contentWidth, maxWidth)
    height: contentHeight

    font.family: "Arial"
    font.pointSize: 20

    textFormat: TextEdit.AutoText
    text: "<b>Hello</b> <i>World!</i><br /><a href=\"http://jucato.logbert.org\">http://jucato.logbert.org</a><br />An editable text area"
    wrapMode: TextEdit.Wrap
    focus: true
    verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
    backgroundVisible: false

    onLinkActivated: Qt.openUrlExternally(link)
}

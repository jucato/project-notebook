#include <QApplication>
#include <QQmlApplicationEngine>

#include "canvas.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<Canvas>("org.jucato", 1, 0, "Canvas");

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

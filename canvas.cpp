#include <QMouseEvent>
#include <QPainter>

#include "canvas.h"

Canvas::Canvas(QQuickItem *parent) : QQuickPaintedItem(parent), m_penColor(QColor(0,0,0)), m_penWidth(10), m_scribbling(false), m_currentMode(Modes::Draw), m_previousMode(Modes::Draw)
{
    setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton);
    m_image = QImage(width(), height(), QImage::Format_ARGB32_Premultiplied);
    m_image.fill(Qt::transparent);
}

void Canvas::setColor(const QColor &newColor)
{
    if (newColor != m_penColor)
    {
        m_penColor = newColor;
        emit colorChanged();
    }
}

void Canvas::setPenWidth(const int &newPenWidth)
{
    if (newPenWidth != m_penWidth)
    {
        m_penWidth = newPenWidth;
        emit penWidthChanged();
    }
}

void Canvas::setScribbling(bool scribbling)
{
    m_scribbling = scribbling;
}

void Canvas::setMode(Canvas::Modes newMode)
{
    if (m_currentMode != newMode)
    {
        m_currentMode = newMode;
        emit modeChanged();
    }
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    if ((m_currentMode == Modes::Draw) || (m_currentMode == Modes::Erase))
    {
        if (event->button() == Qt::LeftButton || event->button() == Qt::RightButton)
        {
            m_lastPoint = event->pos();
            m_scribbling = true;

            if (event->button() == Qt::RightButton)
            {
                m_previousMode = m_currentMode;
                setMode(Modes::Erase);
            }

            emit paintingStarted();
        }
    }
    else
    {
        QQuickPaintedItem::mousePressEvent(event);
    }
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    if (((event->buttons() & Qt::LeftButton) || (event->buttons() & Qt::RightButton)) && (m_scribbling))
        drawLineTo(event->pos());
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    if (((event->button() == Qt::LeftButton) || (event->button() == Qt::RightButton)) && (m_scribbling))
    {
        drawLineTo(event->pos());

        if (event->button() == Qt::RightButton)
        {
            m_currentMode = m_previousMode;
        }

        m_scribbling = false;

        emit paintingStopped();
    }
}

void Canvas::paint(QPainter *painter)
{
    painter->drawImage(boundingRect(), m_image);
}

void Canvas::drawLineTo(const QPointF &endPoint)
{
    if(m_image.isNull()) {
        m_image = QImage(width(), height(), QImage::Format_ARGB32_Premultiplied);
        m_image.fill(Qt::transparent);
    }

    QPainter painter(&m_image);
    if (m_currentMode == Modes::Draw)
        painter.setPen(QPen(m_penColor, m_penWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    else if (m_currentMode == Modes::Erase)
        painter.setPen(QPen(painter.background(), m_penWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawLine(m_lastPoint, endPoint);
    painter.end();

    int rad = (m_penWidth / 2) + 2;
    update(QRectF(m_lastPoint, endPoint).normalized()
                                    .adjusted(-rad, -rad, +rad, +rad).toRect());
    m_lastPoint = endPoint;
}

import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import org.jucato 1.0 as Jucato
import "componentCreation.js" as MyScript

Window {
    visible: true

    minimumWidth: 1024
    minimumHeight: 768

    ControlsToolbar {
        id: controlsbox
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
    }


    MouseArea {
        id: appWindow
        anchors.top: controlsbox.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        Jucato.Canvas {
            id: canvas

            anchors.fill: parent

            color: "#000000"
            penWidth: 5
        }

        onClicked: {
            if (canvas.mode == Jucato.Canvas.Text)
                MyScript.createObjects("TextItem.qml", mouseX, mouseY)
            else if (canvas.mode == Jucato.Canvas.Image) {
                fileDialog.mouseX = mouseX
                fileDialog.mouseY = mouseY
                fileDialog.itemType = "ImageItem.qml"
                fileDialog.visible = true;
            }
            else if (canvas.mode == Jucato.Canvas.File) {
                fileDialog.mouseX = mouseX
                fileDialog.mouseY = mouseY
                fileDialog.itemType = "FileItem.qml"
                fileDialog.visible = true;
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        selectMultiple: false
        property real mouseX: 0
        property real mouseY: 0
        property string itemType: ""

        onAccepted: {
            MyScript.createObjects(itemType, mouseX, mouseY, fileDialog.fileUrl)
        }
        onRejected: {
        }
    }

}


var component;
var item;

function createObjects(itemType, mouseX, mouseY, fileName) {
    component = Qt.createComponent(itemType);
    if (component.status == Component.Ready)
        finishCreation(itemType, mouseX, mouseY, fileName);
    else
        component.statusChanged.connect(finishCreation);
}

function finishCreation(itemType, mouseX, mouseY, fileName) {
    if (component.status == Component.Ready) {
        item = component.createObject(appWindow, {"x": mouseX, "y": mouseY, "source": fileName});

        if (item == null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (component.status == Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}

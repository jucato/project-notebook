import QtQuick 2.0

Image {
    fillMode: Image.PreserveAspectFit
    id: image

    Drag.active: dragArea.drag.active

    MouseArea {
        id: dragArea
        anchors.fill: parent
        drag.target: parent
    }
}
